# Train

This module is used to train an [AI](./ai.py) instance.

<table>
  <tr>
    <td valign="top">
    
## Content
- [Help](#help)
- [Config File](#config-file) 
- [Output](#output)
- [NRAI Output File](#nrai-output-file)
    </td>
  </tr>
</table>

## Usage

Just run the following terminal command to execute the script:

```bash
python train.py -c example_configs/train.yaml
```

#### Help

To print the help use:

```bash
python train.py -h
# or
python train.py --help
 ```

The help info should look like this:

```bash
usage: train.py [-h] [-c CONFIGFILE] [-v VERBOSE]

optional arguments:
  -h, --help            show this help message and exit

config:
  -c CONFIGFILE, --configfile CONFIGFILE
                        A path to a configuration file in yaml syntax
``` 

 
#### Config File

You can specify a yaml configuration file. An example for the simulation can be found here 
[train_simulation.yaml](./example_configs/train_simulation.yaml). To read more about the config file click here
[cli-train_config.md](./cli-train_config.md)

```bash
python train.py -c example_configs/train.yaml
# or
python train.py --configfile example_configs/train.yaml
```


#### Output

After the training the cli outputs the results in the given `model_dir: /path/to/models`. The result contains the following files:

```bash
/path/to/models/
|
+-- model_name/
    |
    +-- events.out.tfevents.<timestamp>.ubuntu # log file for TensorBoard
    +-- history_log_file # can be opened with text editor, 
    |                      # contains training errors and architecture
    +-- model_name.hdf5 # model save file, contains weights
    \-- model_name.nrai # self made config file, contains used image preprocessing for the model 
```

##### NRAI Output File

As described in *Output* the training results also contains a self made config file. Such model_name.nrai files are 
created to save the used image preprocessing. To use the trained model, you have to feed it with the same image shape 
as it was trained on. An example file should look like this [cli-train_nrai.md](./cli-train_nrai.md).