# Converter

This module is used to convert rosbags into faster readable data.
To understand the format of this data see the [example](#output).

<table>
  <tr>
    <td valign="top">
    
## Content
- [Help](#help)
- [Config File](#config-file) 
- [Output](#output)
    </td>
  </tr>
</table>

## Usage

Just run the following terminal command to execute the script:

```bash
python convert.py -c example_configs/convert.yaml
```

### Help

To print the help use:

```bash
python convert.py -h
# or
python convert.py --help
 ```
 
The help info should look like this:

```bash
usage: convert.py [-h] [-c CONFIGFILE] [-v VERBOSE]

optional arguments:
  -h, --help            show this help message and exit

config:
  -c CONFIGFILE, --configfile CONFIGFILE
                        A path to a configuration file in yaml syntax
```
 
Command line arguments *override* the config file values.
 
### Config File

You can specify a yaml configuration file by the following. An example file for the simulation can be found here 
[convert_bags_simulation.yaml](./example_configs/convert_bags_simulation.yaml). To read more about the config file click here 
[cli-convert_config.md](./cli-convert_config.md).

```bash
python convert.py -c /path/to/file/default_config.yaml
# or
python convert.py --configfile /path/to/file/default_config.yaml
```

## Output

Given the rosbag "res/example.bag" and the output dir out/ you get the following output:

```
out/
\-- example/
    +-- example.yaml
    +-- camera_topic_1/
        +-- example_<timestamp1>.jpg
        \-- example_<timestamp2>.jpg
    \-- camera_topic_2/
        +-- example_<timestamp1>.jpg
        \-- example_<timestamp2>.jpg
```

The timestamps will be big integer numbers.

The example.yaml file will look like this:

```yaml
  - timestamp: <timestamp1>
    action_messages:
      /action/topic/1:
        linear:
          y: 0.0
          x: 1.0 # speed value
          z: 0.0
        angular:
          y: 0.0
          x: 0.0
          z: 0.028107548132538795 # steering value
    camera_messages:
    - /camera/topic/1/example_<timestamp1>.jpg
```