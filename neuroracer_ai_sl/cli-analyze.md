# Analyzer

##### Compute Metrics over Dataset

This script can be used to analyze a dataset and compute the featurewise mean and 
standard deviation, as well as a distribution of the steering angles contained within 
the data. Optionally, predictions made by a trained model can be evaluated by augmenting 
the images in the dataset with the true and predicted steering angles.

<table>
  <tr>
    <td valign="top">
    
## Content
- [Help](#help)
- [Config File](#config-file) 
- [Output](#output)
    </td>
  </tr>
</table>

## Usage

#### Important!

**Don't execute** the script with `./analyze.py -c config.yaml` use `python analyze.py -c config.yaml` instead. 
The first call does not work properly under Ubuntu 18.04.

Can only be used on `extracted` data.

Just run the following terminal command to execute the script:

```bash
python analyze.py -c example_configs/analyze.yaml
```

#### Help

To print the help:

```bash
python analyze.py -h
# or
python analyze.py --help
 ```

The help info should look like this:

```bash
usage: analyze.py [-h] [-c CONFIGFILE]

Compute Metrics over Dataset 
This script can be used to analyze a dataset and compute the featurewise mean and 
standard deviation, as well as a distribution of the steering angles contained within 
the data. Optionally, predictions made by a trained model can be evaluated by augmenting 
the images in the datasetwith the true and predicted steering angles

optional arguments:
  -h, --help            show this help message and exit

config:
  -c CONFIGFILE, --configfile CONFIGFILE
                        A path to a configuration file in yaml syntax
``` 

#### Config File

You can specify a yaml configuration file. An example config file for the simulation can be found here 
[analyze_simulation.yaml](./example_configs/analyze_simulation.yaml). To read more about the config file click here: 
[cli-analyze config file](cli-analyze_config.md).

```bash
python analyze.py -c example_configs/analyze.yaml
# or
python analyze.py --configfile example_configs/analyze.yaml
```



#### Output

After the analyzation the cli outputs the results in the given 
`output_dir: /path/to/output/data`. The result contains the following files:

```bash
/path/to/output/data/
|
+-- <extracted_data_name>/
|   |
|   \-- images
|
+-- mean.npy # if save_mean_and_std is True
|
\-- std_dev.npy # if save_mean_and_std is True
```