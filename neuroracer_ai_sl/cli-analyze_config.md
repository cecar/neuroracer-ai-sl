# Analyzer

## Config File

The config file should look like this:

```yaml
model_dir: /path/to/models/<model_name>  # path to trained model
                                         # which contains the .hdf5 and .nrai files
model_name: <model_name>  # trained model name
dual_view: False          # True or False; True if two image input

# Backend parameter for the used backend and learning type
backend_config:
  backend: keras # currently only keras can be used
  learning_type: supervised_learning # in the supervised learning project
                                     # supervised_learning should be used

sources: /path/to/extracted/data # data used for training
output_dir: /path/to/output/data # dir, used for output files
batch_size: 256         # size of the batches

compute_metrics: True   # computes mean absolute error and mean squared error, 
                        # computes images mean and standard deviation
                        # and prints the results in console
                        
save_mean_and_std: True # saves the image mean and standard deviation as .npy files

draw_eval_arrows: True  # draws steering arrows on source data

predict: False          # used for stdout prints and 
                        # draws predicted steering arrows on source data
                        # needs lot of computation time

topics:
  cameras: ["/zed/left/image_raw_color/compressed"] # list of ros topics for cameras 
  actions: ["/nr/engine/input/actions"]             # list of ros topics for actions
```

### Model Dir

You can specify a directory where the data of the neural network will be stored.

### Model Name

You can specify the name of the model.

### Dual View

If the architecture uses two input images, you can set the dual view flag,
so the data gets processed correctly.

### Backend Config

Defines which backend (currently only "keras") and learning type (here in the supervised 
learning project mostly "supervised_learning") should be used.

### Batch Size

Defines the used batch size.

### Sources

Defines the used source data.

### Output Dir

Defines the output directory.

### Compute Metrics

Prints the computed mean absolute error, mean squared error, images mean and standard 
deviation in console.

### Save mean and std

Saves the image mean and standard deviation as .npy files in the output directory. 
Can be used for normalization.

### Draw eval arrows

Draws true steering arrows on source data.

View example image (created with `predict: True`):

![Draw eval arrows](./images/cli/analyze/example01.jpg)

### Predict

This attribute is used for console printing and arrow drawing. But be careful, 
needs lot of `computation time`.
