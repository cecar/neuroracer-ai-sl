from neuroracer_ai.factories.abstract import EBackends
from neuroracer_ai.processors import ImageProcessor, ImageProcessingParameters
from neuroracer_ai.suites import Image2DProcessorSuite, ManualMappingProcessorSuite
from neuroracer_ai.utils.backend_solver import get_backend

from neuroracer_ai_sl.ai import AI, TrainParameters

imports = [
    "AI",
    "TrainParameters",
    "ImageProcessor",
    "ImageProcessingParameters",
    "Image2DProcessorSuite",
    "ManualMappingProcessorSuite"
]

_BACKEND = get_backend()

if _BACKEND == EBackends.KERAS:
    try:
        from neuroracer_ai_sl.models.keras.architectures import KerasArchitectures

        imports.append("KerasArchitectures")
    except ImportError as e:
        print("[INFO] Required module for defined backend Keras not found:", e)

elif _BACKEND == EBackends.PYTORCH:
    try:
        from neuroracer_ai_sl.models.pytorch_backend_sl import PyTorchArchitectures

        imports.append("PyTorchArchitectures")
    except ImportError as e:
        print("[INFO] Required module for defined backend PyTorch not found:", e)

else:
    raise NotImplementedError('Define backend not found: ', _BACKEND)

__all__ = imports
