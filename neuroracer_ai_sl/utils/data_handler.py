import os
import functools

from multiprocessing import Pool
from cv2 import imwrite
from os import makedirs, path
from ruamel.yaml import YAML
from tqdm import tqdm

from neuroracer_ai.utils.config_handler import NB_CPU


def get_container_name(rosbag_path):
    """
    Transforms paths like 'res/example.bag' into 'example'.

    :param rosbag_path: The path to a rosbag to transform.
    :returns: The container_name of a rosbag path.
    """
    return os.path.splitext(os.path.basename(rosbag_path))[0]


def get_data_name(container_name, timestamp, occurrence_counter):
    """
    Creates a unique name for this data.

    :param container_name: The name of the container containing this data
    :type container_name: str
    :param timestamp: The timestamp of the data
    :type timestamp: int
    :param occurrence_counter: a dictionary containing information about
                               the number of occurrences of the data_name
    :type occurrence_counter: dict
    :returns: A new data_name which is unique
    :rtype: str
    """

    data_name = "{container_name}_{timestamp}".format(
        container_name=container_name,
        timestamp=str(timestamp))

    if data_name in occurrence_counter:
        occurrence_counter[data_name] += 1
    else:
        occurrence_counter[data_name] = 0

    counter = occurrence_counter[data_name]

    return "{data_name}_{counter}".format(data_name=data_name, counter=counter)


def write_camera_msgs(sub_destination, container_name, snapshot_list, verbose):
    """
    Writes images into the destination directory.

    :param sub_destination: The directory in which the pictures and the yaml
                            file should be created.
    :type sub_destination: str
    :param container_name: The container_name of the extracted data
    :type container_name: str
    :param snapshot_list: The snapshot_list with the images to write.
    :type snapshot_list: list[neuroracer_ai.utils.ImageSnapshot]
    :param verbose: verbosity of this call. 0: no prints, 1: prints
    :type verbose: int
    """

    def maybe_make_topic_dir(subdir, topic):
        # remove leading slash from topic,  or joining paths wont work
        topic = topic.lstrip(path.sep)
        dir_path = path.join(subdir, topic)
        if not path.exists(dir_path):
            makedirs(dir_path)
        return dir_path

    occurence_counter = {}

    if verbose > 0:
        desc = "writing images into '{}'.".format(sub_destination)
        progress_func = functools.partial(tqdm, desc=desc)
    else:
        def progress_func(x):
            return x

    for snapshot in progress_func(snapshot_list):
        data_name = get_data_name(container_name, snapshot.timestamp, occurence_counter)
        for topic, uncompressed_image in snapshot.get_uncompressed_camera_msgs().items():
            dir_path = maybe_make_topic_dir(sub_destination, topic)
            filename = path.join(dir_path, "{}.jpg".format(data_name))
            imwrite(filename, uncompressed_image)


def write_action_msgs(sub_destination, container_name, snapshot_list, verbose):
    """
    Writes a yaml file with movement information into the provided destination
    directory.

    :param sub_destination: The directory in which the pictures and the yaml
                            file should be created.
    :type sub_destination: str
    :param container_name: The container_name of the extracted data
    :type container_name: str
    :param snapshot_list: The snapshot list with the movement information
    :type snapshot_list: list[neuroracer_ai.utils.ImageSnapshot]
    :param verbose: verbosity of this call. 0: no prints, 1: prints
    :type verbose: int
    """

    occurrence_counter = {}
    data = []

    filename = path.join(sub_destination, container_name + ".yaml")

    if verbose > 0:
        print("writing movement information into '{}'.".format(filename))

    for snapshot in snapshot_list:
        data_name = get_data_name(container_name, snapshot.timestamp, occurrence_counter)
        d = {"camera_messages": [path.join(topic, "{}.jpg".format(data_name))
                                 for topic in snapshot.camera_msgs.keys()],
             "action_messages": snapshot.action_msgs,
             "timestamp": str(snapshot.timestamp)}

        data.append(d)

    yaml = YAML()
    with open(filename, 'a') as outfile:
        # for further information see:
        # https://stackoverflow.com/questions/12470665/how-can-i-write-data-in-yaml-format-in-a-file
        yaml.dump(data, outfile)  # todo: handle flow style

    """


    :param destination: The directory in which the pictures and the yaml file should be created.
    :param as_archive: if true snapshot_item will be written into a zip file
    :param snapshot_item: Todo
    :type snapshot_item tuple(str, str)
    :type as_archive: bool
    """


def write_snapshot_iterable(snapshot_item, destination, verbose):
    """
    Writes an snapshot list into a directory specified by <destination>.
    After this function call the directory should contain the pictures
    in the snapshot list as jpg files and a yaml file.
    This function works even if there are snapshots with the same timestamp.
    The snapshots with same timestamp are numbered in the same order as they
    appear in the snapshot_list.

    :param snapshot_item: Snapshots and their corresponding container name
    :type snapshot_item: tuple[str, list]
    :param destination: The directory in which the pictures and the yaml
                        file should be created
    :type destination: str
    :param verbose: Set to 1 for verbose output, 0 for no output
    :type verbose: int
    :return: None
    :rtype: None
    """

    container_name, snapshot_list = snapshot_item
    sub_destination = path.join(destination, container_name)

    if not path.isdir(destination):
        makedirs(destination)

    if not path.isdir(sub_destination):
        makedirs(sub_destination)

    write_action_msgs(sub_destination, container_name, snapshot_list, verbose=verbose)
    write_camera_msgs(sub_destination, container_name, snapshot_list, verbose=verbose)


def write_snapshot_dict(snapshot_dict, destination, verbose, nb_cpu=NB_CPU):
    """
    Writes the given snapshot_dict into the destination directory

    :param snapshot_dict: A dictionary with the container names as keys and
                          the snapshot list as values.
    :param snapshot_dict: dict[str: list]
    :param destination: A path to a directory where the extracted data is
                        to be written
    :param destination:str
    :param verbose: 0: do not print status information;
                    1: print status information
    :param verbose: int
    :param nb_cpu: number of cores to use
    :param nb_cpu: int
    :return: None
    :rtype: None
    """
    if nb_cpu > 1:
        pool = Pool(processes=NB_CPU)
        write_snapshot_item_with_arguments = functools.partial(write_snapshot_iterable,
                                                               destination=destination,
                                                               verbose=verbose)
        pool.map(write_snapshot_item_with_arguments, snapshot_dict.items())
        pool.close()
        pool.join()
    else:
        for item in snapshot_dict.items():
            write_snapshot_iterable(item, destination=destination, verbose=verbose)
