#!/usr/bin/env python
import math
import multiprocessing
import os
from collections import defaultdict

import cv2
import numpy as np
from neuroracer_ai.utils.abstract import AbstractGenerator
from neuroracer_ai.utils.snapshot import ImageSnapshot
from neuroracer_common.version_solver import get_ABC
from ruamel.yaml import YAML

ABC = get_ABC()

IMAGE_FILE_EXTENSION = ".jpg"


class AbstractDataReader(ABC):
    def __init__(self):
        """
        Required common attributes for data readers.

        """
        self.batch_size = None  # convenience property
        self.n_total_data = None  # convenience property
        self.batches_per_epoch = None  # convenience property
        self.data_list = None  # convenience property

    def _prepare(self, n_total_data=None):
        """
        Initializes count of total data and amount of batches to process during each epoch.

        :param n_total_data: TODO
        :type n_total_data: int

        """
        if isinstance(n_total_data, int):
            # manually override, use with caution
            self.n_total_data = n_total_data
        else:
            # builtin pipeline
            self.n_total_data = len(self.data_list)

        self.batches_per_epoch = int(math.ceil(float(self.n_total_data) / float(self.batch_size)))


class ExtractedDataReader(AbstractGenerator, AbstractDataReader):

    def __init__(self, main_dir, batch_size, shuffle=False, nb_cpu=1, return_dict=False, loop_indefinitely=True):
        """
        Creates a generator object which reads extracted data from all
        subdirectories in the specified main directory and returns
        ImageSnapshot objects in batches. For every subdirectory, a snapshot
        information yaml file is read and parsed and ImageSnapshot objects are
        created and yielded. Data can be shuffled before generation and the
        generator can loop forever if needed.
        (keras model.fit_generator needs this)

        :param main_dir: Main directory, contains subdirectories with yaml
                         files and images
        :type main_dir: str
        :param batch_size: how many snapshots are returned per generator call
        :type batch_size: int
        :param shuffle: whether or not to shuffle the order of the snapshots
        :type shuffle: bool
        :param nb_cpu: number of cpu cores to use. Multiprocessing is used,
                       when nb_cpu > 1
        :type nb_cpu: int
        :param return_dict: When True, returns snapshots grouped by subdir
                            they originate from
        :type return_dict: bool
        :param loop_indefinitely: if True, the generator loops around and
                                  yields batches forever
        :type loop_indefinitely: bool
        :return: A generator yielding ImageSnapshots
        :rtype: generator
        """

        super(ExtractedDataReader, self).__init__()
        self.main_dir = main_dir
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.nb_cpu = nb_cpu
        self.return_dict = return_dict
        self.loop_indefinitely = loop_indefinitely
        self._prepare_generator()

    def _prepare_generator(self):
        # we want the "batches_per_epoch" and "data_list" variables set before the first call to "next"
        sub_dirs = [os.path.join(self.main_dir, d) for d in os.listdir(self.main_dir)]
        self.data_list = self._get_snapshot_data(sub_dirs)
        self._prepare()

    def _get_snapshot_data(self, sub_dirs):
        """
        Reads multiple directories and returns all snapshot data contained
        within the yaml configuration files found in each directory.
        ImageSnapshot objects can be cerated elsewhere using this data.

        :param sub_dirs: directories that are being read for snapshot data
        :return: list of snapshot data
        """

        # pool still needed for analyze.py and augment.py
        if self.nb_cpu > 1:
            pool = multiprocessing.Pool(processes=self.nb_cpu)
            snapshot_data = pool.map(_get_snapshot_data_from_sub_dir, sub_dirs)
            pool.close()
            pool.join()
        else:
            snapshot_data = [_get_snapshot_data_from_sub_dir(sub_dir) for sub_dir in sub_dirs]

        return [snap_data
                for snapshot_data_list in snapshot_data
                for snap_data in snapshot_data_list]

    def _create_snapshot_batch(self, snap_data_batch):
        """
        Creates a batch of ImageSnapshot objects from a batch of snapshot data

        :param snap_data_batch: snapshota data used to create
                                ImageSnapshot objects
        :return: list of Snapshot objects
        """

        # pool still needed for analyze.py and augment.py
        if self.nb_cpu > 1:
            pool = multiprocessing.Pool(processes=self.nb_cpu)
            snapshots = pool.map(_create_snapshot, snap_data_batch)
            pool.close()
            pool.join()
            return snapshots
        else:
            return [_create_snapshot(snap_data) for snap_data in snap_data_batch]

    @staticmethod
    def _group_snapshots_by_dir_name(dirs, snaps):
        snapshots_grouped_by_subdir = defaultdict(list)

        for dir_, snap in zip(dirs, snaps):
            snapshots_grouped_by_subdir[dir_].append(snap)

        return snapshots_grouped_by_subdir

    def _index_needs_reset(self):
        return self.loop_indefinitely and (self.index + self.batch_size >= self.n_total_data)

    def send(self):
        if not os.path.isdir(self.main_dir):
            raise IOError("directory '{}' not found.".format(self.main_dir))

        while self.index < self.n_total_data:
            if self.index == 0 and self.shuffle:
                np.random.shuffle(self.data_list)

            batch = self.data_list[self.index:self.index + self.batch_size]
            self.index = 0 if self._index_needs_reset() else self.index + self.batch_size

            snapshot_batch = self._create_snapshot_batch(batch)

            if self.return_dict:
                folder_names = [os.path.split(sub_dir_path)[1]
                                for sub_dir_path, _, _, _ in batch]
                return self._group_snapshots_by_dir_name(folder_names, snapshot_batch)
            else:
                return list(snapshot_batch)

        # raise StopIteration when generator is exhausted
        self.throw()

    def __getitem__(self, idx):
        if not os.path.isdir(self.main_dir):
            raise IOError("directory '{}' not found.".format(self.main_dir))

        if idx == 0 and self.shuffle:
            np.random.shuffle(self.data_list)

        batch = self.data_list[idx * self.batch_size:(idx + 1) * self.batch_size]

        snapshot_batch = self._create_snapshot_batch(batch)

        if self.return_dict:
            folder_names = [os.path.split(sub_dir_path)[1]
                            for sub_dir_path, _, _, _ in batch]
            return self._group_snapshots_by_dir_name(folder_names, snapshot_batch)
        else:
            return list(snapshot_batch)


def _create_snapshot(snapshot_data):
    """
    Takes snapshot data and instantiates a snapshot object with it.
    Snapshota data is made up of multiple entries, each containing the
    following:
      - path to base directory containing all images
      - relative image paths (one for every camera topic)
      - action messages
      - timestamp

    ATTENTION: This function must be declared outside of the RosBagReader
    class, otherwise multiprocessing will not work. Functions passed to a
    multiprocessing pool must be serializable, which class and instance
    methods are not in Python 2.7

    :param snapshot_data: data to create snapshot object from
                          - path to base directory containing all images
                          - relative image paths (one for every camera topic)
                          - action messages as dict
                          - timestamp
    :type snapshot_data: tuple[str, str, dict, str]
    :return: ImageSnapshot object
    :rtype: ImageSnapshot
    """
    sub_dir_name, relative_image_paths, action_msgs, timestamp = snapshot_data
    camera_msgs = {}

    for rel_image_path in relative_image_paths:
        # os.path wont join correctly if leading slash is included
        topic = rel_image_path.rsplit(os.sep, 1)[0]
        rel_img_path_wo_leading_slash = rel_image_path.strip(os.path.sep)

        abs_img_path = os.path.join(sub_dir_name, rel_img_path_wo_leading_slash)
        image = cv2.imread(abs_img_path)
        compressed_img = cv2.imencode(IMAGE_FILE_EXTENSION, image)[1]
        camera_msgs[topic] = compressed_img

    return ImageSnapshot(camera_msgs, action_msgs, timestamp)


def _get_snapshot_data_from_sub_dir(sub_dir):
    """
    Reads a snapshot information yaml file from a directory and creates a list
    of every entry in the yaml file. This raw data can be then used elsewhere
    to create an ImageSnaphsot object. This information includes:
                          - path to base directory containing all images
                          - relative image paths (one for every camera topic)
                          - action messages
                          - timestamp


    ATTENTION: This function must be declared outside of the RosBagReader
    class, otherwise multiprocessing will not work. Functions passed to a
    multiprocessing pool must be serializable, which class and instance
    methods are not in Python 2.7


    :param sub_dir: directory containing yaml file
    :type sub_dir: str
    :return: snapshot information defined in yaml file
    :rtype: list[str, list, dict, str]
    """
    sub_dir_name = os.path.split(sub_dir)[1]
    yaml_file = os.path.join(sub_dir, sub_dir_name + ".yaml")
    yaml = YAML()
    snapshot_data_list = []

    with open(yaml_file) as data_file:
        yaml_content = yaml.load(data_file)

    for counter, yaml_entry in enumerate(yaml_content):
        # TODO ISSUE  # 9: https://gitlab.com/NeuroRace/neuroracer-ai/issues/9
        # TODO ruamel yaml_entries are CommentedMaps and no dicts, cast to dict should be done
        # TODO but maybe CommentedMaps are needed, here cast not possible,
        # TODO because the multithreading pool says: CommentedMap not a hashable type
        camera_msgs = yaml_entry["camera_messages"]
        action_msgs = yaml_entry["action_messages"]
        timestamp = yaml_entry["timestamp"]
        snapshot_data_list.append((sub_dir, camera_msgs, action_msgs, timestamp))

    return snapshot_data_list
