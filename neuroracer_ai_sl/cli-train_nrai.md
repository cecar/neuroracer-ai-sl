# Train

## NRAI Output File

An example file should look like this: 

```bash
ai_parameters:
  processor_suite: !!python/object:neuroracer_ai.suites.image.Image2DProcessorSuite
    _image_processor: !!python/object:neuroracer_ai.processors.image.ImageProcessor
    
      _color_space_conv_func: null # opencv color convert function enum
                                   # should be the same as at the bottom
      
      _crop_parameters: &id001 !!python/object:neuroracer_ai.processors.image.ImageCropParameters { # definition of the
        bottom: 0.0, left: 0.0, relative: true, right: 0.0, top: 0.5}                               # crop parameter
      
      _decode_image: true # image processor decompressing option
                          # should be the same as at the bottom
      
      _flip_option: null  # image preocessor flip option
                          # should be the same as at the bottom
      
      _raw_params: !!python/object:neuroracer_ai.processors.image.ImageProcessingParameters
        crop_parameters: *id001            # id for the crop parameter
        
        decode_image: true                 # image decompressing
        
        flip_option: null                  # flipping option
        
        normalize: 255.0                   # narmalization value in ImageProcessingParameters, 
                                           # should be the same as at the bottom
                                           
        opencv_color_space_conv_func: null # no gray scaling or so
        
        resize_parameters: &id002 !!python/object:neuroracer_ai.processors.image.ImageResizeParameters { # definition of the
          interpolation_method: 3, relative_scaling: false, x_scaling: 134, y_scaling: 37}               # resize parameter
          
        subtract_mean: null # mean substraction in ImageProcessingParameters
                            # should be the same as at the bottom
        
      _resize_parameters: *id002 # id for the resize parameter
      _subtract_mean: null       # no mean substraction
      _use_normalization: 255.0  # used normalization value
```