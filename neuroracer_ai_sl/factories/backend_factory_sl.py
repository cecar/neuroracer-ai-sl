import sys

from neuroracer_ai.factories import AbstractBackendFactory, EBackends
from neuroracer_ai.utils.backend_solver import get_backend


class BackendFactorySL(AbstractBackendFactory):
    """
    Backend factory to create supervised learning model instances
    """

    def __init__(self):
        super(BackendFactorySL, self).__init__()

    @staticmethod
    def build():
        """
        Build method to get an implementation of a backend based on the given config

        :return: backend instance
        :type:   BaseModel
        """

        backend = get_backend()

        # checks if SL keras backend
        if backend == EBackends.KERAS:
            from neuroracer_ai_sl.models.keras_backend_sl import KerasModelSL
            from keras import __version__

            model = KerasModelSL()

            sys.stderr.write('[INFO] Using Keras v' + str(__version__) + ' as backend.\n')

        # checks if SL pytorch backend
        elif backend == EBackends.PYTORCH:
            from neuroracer_ai_sl.models.pytorch_backend_sl import PyTorchModelSL
            from torch import __version__

            model = PyTorchModelSL()

            sys.stderr.write('[INFO] Using PyTorch v' + str(__version__) + ' as backend.\n')

        # raise ValueError if model couldn't be created
        else:
            raise ValueError("couldn't create backend: {}".format(backend))

        sys.stderr.write(
            '[INFO] This Model is for TRAINING and development predictions. Use the execution-only AI package for deployment.\n')

        return model
