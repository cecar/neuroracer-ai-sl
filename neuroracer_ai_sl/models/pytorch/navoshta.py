from __future__ import division

from torch.nn import Conv2d, CrossEntropyLoss, Dropout, Linear, MaxPool2d, MSELoss, ReLU, Sequential, Softmax
from torch.nn.init import xavier_uniform_, zeros_
from torch.optim import Adadelta, Adam

from neuroracer_ai_sl.models.pytorch.abstract import AbstractNet


class NavoshtaNetRegression(AbstractNet):
    """
    Behavior Cloning based on NVIDIA approach for less powerful hardware,
    see https://github.com/navoshta/behavioral-cloning
    """

    def __init__(self, input_shape, learning_rate=1e-04):
        """
        Initialize Network Layers

        Default parameter of PyTorch for learning_rate is 1e-03 but NVIDIA used 1e-04,
        so use it in this class as default value.

        :param input_shape: TODO
        :param learning_rate: TODO
        """

        # pass params required by parent class
        super(NavoshtaNetRegression, self).__init__(input_shape, learning_rate)

        # split input according to your network's design,
        # in this case a single incoming image split into its height, width and depth
        # and passed accordingly to the network
        depth = input_shape[0]
        height = input_shape[1]
        width = input_shape[2]

        # define network architecture
        # in PyTorch, the layer's name is equal to the property of the container
        # e.g. the name of self.layer_1 is layer_1

        # input layer as convolutional with ReLU activation and max pooling
        self.layer_1 = Sequential(
            Conv2d(in_channels=depth, out_channels=16, kernel_size=(3, 3)),
            ReLU(),
            MaxPool2d(kernel_size=(2, 2)))

        # calc output of conv layer for dynamic image size adaption in first linear layer
        height, width = self.conv2d_output_shape((height, width), kernel_size=(3, 3))
        # calc output of maxpool layer for dynamic image size adaption in first linear layer
        height, width = self.maxpool2d_output_shape((height, width), kernel_size=(2, 2))

        # 2nd convolutional layer with ReLU activation and max pooling
        self.layer_2 = Sequential(
            Conv2d(in_channels=16, out_channels=32, kernel_size=(3, 3)),
            ReLU(),
            MaxPool2d(kernel_size=(2, 2)))

        # calc output of conv layer for dynamic image size adaption in first linear layer
        height, width = self.conv2d_output_shape((height, width), kernel_size=(3, 3))
        # calc output of maxpool layer for dynamic image size adaption in first linear layer
        height, width = self.maxpool2d_output_shape((height, width), kernel_size=(2, 2))

        # 3rd convolutional layer with ReLU activation and max pooling
        self.layer_3 = Sequential(
            Conv2d(in_channels=32, out_channels=64, kernel_size=(3, 3)),
            ReLU(),
            MaxPool2d(kernel_size=(2, 2)))

        # calc output of conv layer for dynamic image size adaption in first linear layer
        height, width = self.conv2d_output_shape((height, width), kernel_size=(3, 3))
        # calc output of maxpool layer for dynamic image size adaption in first linear layer
        height, width = self.maxpool2d_output_shape((height, width), kernel_size=(2, 2))

        # 1st fully connected layer with Dropout
        self.layer_4 = Sequential(
            Linear(in_features=64 * height * width, out_features=500),
            Dropout(p=0.5))

        # 2nd fully connected layer with Dropout
        self.layer_5 = Sequential(
            Linear(in_features=500, out_features=100),
            Dropout(p=0.25))

        # Note:
        # Even each of the last sequences only consist of one layer, they are designed as sequences to make it easier
        # to use a generic function for layer visualization by our framework's implementation.
        # This may change in future.

        # 3rd fully connected layer
        self.layer_6 = Sequential(
            Linear(in_features=100, out_features=20)
        )

        # output layer
        self.layer_7 = Sequential(
            Linear(in_features=20, out_features=1)
        )

        # initialize weights of layers (optional)
        self.apply(self._weights_init)

    def forward(self, x):
        """
        Input-forwarding through the layers of the network

        :param x: TODO
        :return: TODO
        """

        x = self.layer_1(x)
        x = self.layer_2(x)
        x = self.layer_3(x)
        x = x.reshape(x.size(0), -1)  # keras.Flatten() equivalent
        x = self.layer_4(x)
        x = self.layer_5(x)
        x = self.layer_6(x)
        x = self.layer_7(x)

        return x

    def get_optimizer(self):
        """
        Optimizer for network

        :return: torch.optim.Adam
        """
        return Adam(self.parameters(), lr=self.learning_rate)

    def get_loss_function(self):
        """
        Loss function for network

        :return: torch.nn.MSELoss
        """
        return MSELoss(reduction='mean')

    @staticmethod
    def _weights_init(m):
        """
        Initialize layers (optional).

        :param m:
        """
        if isinstance(m, Conv2d) or isinstance(m, Linear):
            # use Xavier initialization for all convolutional and fully connected layers
            xavier_uniform_(m.weight)
            zeros_(m.bias)


class NavoshtaNetClassification(AbstractNet):
    """
    Behavior Cloning based on NVIDIA approach for less powerful hardware adjusted for classification,
    see https://github.com/navoshta/behavioral-cloning for origin (regression)
    """

    def __init__(self, input_shape, learning_rate=1.0):
        """
        Initialize Network Layers

        :param input_shape: TODO
        :param learning_rate: TODO
        """

        # pass params required by parent class
        super(NavoshtaNetClassification, self).__init__(input_shape, learning_rate)

        # split input according to your network's design,
        # in this case a single incoming image split into its height, width and depth
        # and passed accordingly to the network
        depth = input_shape[0]
        height = input_shape[1]
        width = input_shape[2]

        # define network architecture
        # in PyTorch, the layer's name is equal to the property of the container
        # e.g. the name of self.layer_1 is layer_1

        # input layer as convolutional with ReLU activation and max pooling
        self.layer_1 = Sequential(
            Conv2d(in_channels=depth, out_channels=16, kernel_size=(3, 3)),
            ReLU(),
            MaxPool2d(kernel_size=(2, 2)))

        # calc output of conv layer for dynamic image size adaption in first linear layer
        height, width = self.conv2d_output_shape((height, width), kernel_size=(3, 3))
        # calc output of maxpool layer for dynamic image size adaption in first linear layer
        height, width = self.maxpool2d_output_shape((height, width), kernel_size=(2, 2))

        # 2nd convolutional layer with ReLU activation and max pooling
        self.layer_2 = Sequential(
            Conv2d(in_channels=16, out_channels=32, kernel_size=(3, 3)),
            ReLU(),
            MaxPool2d(kernel_size=(2, 2)))

        # calc output of conv layer for dynamic image size adaption in first linear layer
        height, width = self.conv2d_output_shape((height, width), kernel_size=(3, 3))
        # calc output of maxpool layer for dynamic image size adaption in first linear layer
        height, width = self.maxpool2d_output_shape((height, width), kernel_size=(2, 2))

        # 3rd convolutional layer with ReLU activation and max pooling
        self.layer_3 = Sequential(
            Conv2d(in_channels=32, out_channels=64, kernel_size=(3, 3)),
            ReLU(),
            MaxPool2d(kernel_size=(2, 2)))

        # calc output of conv layer for dynamic image size adaption in first linear layer
        height, width = self.conv2d_output_shape((height, width), kernel_size=(3, 3))
        # calc output of maxpool layer for dynamic image size adaption in first linear layer
        height, width = self.maxpool2d_output_shape((height, width), kernel_size=(2, 2))

        # 1st fully connected layer with Dropout
        self.layer_4 = Sequential(
            Linear(in_features=64 * height * width, out_features=500),
            Dropout(p=0.5))

        # 2nd fully connected layer with Dropout
        self.layer_5 = Sequential(
            Linear(in_features=500, out_features=100),
            Dropout(p=0.25))

        # Note:
        # Even this sequences only consist of one layer, it is designed as sequences to make it easier
        # to use a generic function for layer visualization by our framework's implementation.
        # This may change in future.

        # 3rd fully connected layer
        self.layer_6 = Sequential(
            Linear(in_features=100, out_features=20)
        )

        # output layer
        self.layer_7 = Sequential(
            Linear(in_features=20, out_features=10),
            # for further information on `dim` parameter, see
            # https://discuss.pytorch.org/t/implicit-dimension-choice-for-softmax-warning/12314
            # for reference example of mnist, see
            # https://towardsdatascience.com/handwritten-digit-mnist-pytorch-977b5338e627
            Softmax(dim=1)
        )

        # initialize weights of layers (optional)
        self.apply(self._weights_init)

    def forward(self, x):
        """
        Input-forwarding through the layers of the network

        :param x: TODO
        :return: TODO
        """

        x = self.layer_1(x)
        x = self.layer_2(x)
        x = self.layer_3(x)
        x = x.reshape(x.size(0), -1)  # keras.Flatten() equivalent
        x = self.layer_4(x)
        x = self.layer_5(x)
        x = self.layer_6(x)
        x = self.layer_7(x)

        return x

    def get_optimizer(self):
        """
        Optimizer for network

        :return: torch.optim.Adadelta
        """
        return Adadelta(self.parameters(), lr=self.learning_rate)

    def get_loss_function(self):
        """
        Loss function for network

        :return: torch.nn.CrossEntropyLoss
        """
        return CrossEntropyLoss(reduction='sum')

    @staticmethod
    def _weights_init(m):
        """
        Initialize layers (optional).

        :param m:
        """
        if isinstance(m, Conv2d) or isinstance(m, Linear):
            # use Xavier initialization for all convolutional and fully connected layers
            xavier_uniform_(m.weight)
            zeros_(m.bias)
