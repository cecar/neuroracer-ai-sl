"""
This module is everything a user has to import to use the library. The main api
class is AI. It contains every available call. The other classes are all helper
classes to simplify the configuration.
"""

from __future__ import print_function

from os.path import join
import math
from neuroracer_ai.ai import AI as BASE_AI
from neuroracer_ai.factories.abstract import EBackends
from neuroracer_ai.models import Trainable
from neuroracer_ai.suites import EmptyProcessorSuite
from neuroracer_ai.utils import file_handler as fh
from neuroracer_ai.utils.backend_solver import get_backend

from neuroracer_ai_sl.factories import BackendFactorySL

BACKEND = get_backend()


class TrainParameters:
    """
    Parameters which are used for the training.
    """

    def __init__(self, train_data_generator, validation_data_generator, data_processor, processor_suite,
                 shuffle, batch_size, steps_per_epoch, validation_steps, architecture,
                 checkpoint_dir=None, use_checkpoints=True, use_builtin_pipeline=True):
        """
        :param train_data_generator: Generator returning training data and label batches
        :type train_data_generator: neuroracer_ai_sl.utils.ExtractedDataReader
        :param validation_data_generator: Generator returning validation data and label batches
        :type validation_data_generator: ExtractedDataReader
        :param data_processor: SnapshotProcessorSuite to process train/validation data and label batches from
                               train_data_train_data_generator & validation_data_generator
        :type data_processor: neuroracer_ai.suites.SnapshotProcessorSuite
        :param processor_suite: processing suite which processes every input
                for training and prediction. If you need something fancier than
                the predefined ones you can use the
                neuroracer_ai.suites.ManualMappingProcessorSuite. It allows you
                to completely customize the routing of the data to the
                specified processors. You should never implement your own
                ProcessorSuite class! This will cause a failure while loading
                the model next time.
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        :param shuffle: Shuffle
        :type shuffle: bool
        :param batch_size: Size of the batches which are used. Defaults to 120
        :type batch_size: int
        :param steps_per_epoch: How many batches does the training data
                                generator have to return, before one epoch
                                is complete
        :type steps_per_epoch: int
        :param validation_steps How many batches does the validation data
                                generator have to return, before one
                                validation epoch is complete
        :type validation_steps: int
        :param checkpoint_dir: Path in which checkpoints are written
        :type validation_steps: int
        :param architecture: An architecture building function
        :type architecture: Callable
        :param use_checkpoints: Enables the saving of checkpoints.
                                Defaults to True
        :type use_checkpoints: bool
        :param use_builtin_pipeline: Whether to use the builtin snapshot processing pipeline or
                to pass the given training and evaluation keras.utils.Sequence directly to Keras
        :type use_builtin_pipeline: bool
        """

        self.train_data_generator = train_data_generator
        self.validation_data_generator = validation_data_generator
        self.data_processor = data_processor
        self.processor_suite = processor_suite
        self.shuffle = shuffle
        self.batch_size = batch_size
        self.steps_per_epoch = steps_per_epoch
        self.validation_steps = validation_steps
        self.checkpoint_dir = checkpoint_dir
        self.architecture = architecture
        self.use_checkpoints = use_checkpoints
        self.use_builtin_pipeline = use_builtin_pipeline


class AI(BASE_AI):
    @staticmethod
    def create(name, model_dir, architecture_func, architecture_func_params, is_regression=True,
               processor_suite=EmptyProcessorSuite()):
        """
        Creates a new model and based on it a new working instance of the ai
        class.

        :param name: name of which the ai/ model uses to save
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param architecture_func: architecture which is used by the model
        :type architecture_func: Architecture
        :param architecture_func_params: parameters which are passed to the
                architecture constructor
        :type architecture_func_params: Dict[str, Any]
        :param is_regression:
        :param processor_suite: processing suite which processes every input
                for training and prediction. If you need something fancier than
                the predefined ones you can use the
                neuroracer_ai.suites.ManualMappingProcessorSuite. It allows you
                to completely customize the routing of the data to the
                specified processors. You should never implement your own
                ProcessorSuite class! This will cause a failure while loading
                the model next time.
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        :return: constructed AI instance
        :rtype: neuroracer_ai.AI
        """
        model = BackendFactorySL.build()
        model.create(architecture_func=architecture_func, architecture_func_params=architecture_func_params,
                     is_regression=is_regression)

        return AI(
            model=model,
            name=name,
            model_dir=model_dir,
            processor_suite=processor_suite)

    @staticmethod
    def load(name, model_dir, trainable=True):
        """
        Loads the given model and returns a working instance of the ai class.
        Setting trainable to false decreases the memory usage significantly!
        But should only be used if you only want to predict with an already
        trained nn.
        If you try to train an untrainable nn a Error will be thrown!

        :param: name: name of the ai/ model
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param trainable: loads the model in a way which allows it to be
                          trained.
        :type trainable: bool
        :return: constructed AI instance
        :rtype: neuroracer_ai.AI
        """

        ai_parameters = fh.load_ai_parameters(fh.create_config_path(model_dir, name))

        model = BackendFactorySL.build()
        model.load(model_path=fh.create_model_path(model_dir, name), trainable=trainable)

        return AI(
            name=name,
            model_dir=model_dir,
            model=model,
            **ai_parameters)

    def train(self, train_parameters, verbose=0):
        """
        Trains the model with the given data and returns the history of it.

        :param train_parameters: parameters which should be used for the
                training
        :type train_parameters: ai.TrainParameters
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: History object of the training
        :rtype: Dict
        """

        if not isinstance(self._model, Trainable):
            raise TypeError("The chosen backend doesn't support training!")

        train_parameters.processor_suite = self._processor_suite

        if train_parameters.use_checkpoints:
            self._write_config(directory=self._model_dir, overwrite=True)

        # pre training information output
        if verbose > 0:
            self._pre_training_console_log(train_parameters=train_parameters)

        # train and retrieve history
        history = self._model.train(
            train_parameters=train_parameters,
            checkpoint_path=fh.create_model_path(self._model_dir, self._name),
            verbose=verbose)

        log_file_path = join(self._model_dir, self._name + '_train.log')

        # post training information output
        if verbose > 0:
            self._post_training_console_log(log_file_path)

        return self._model.log_history(history=history,
                                       architecture_func=train_parameters.architecture,
                                       model_name=self._name,
                                       destination=log_file_path)

    @staticmethod
    def _pre_training_console_log(train_parameters):
        total_training_samples = train_parameters.train_data_generator.n_total_data
        total_validation_samples = train_parameters.validation_data_generator.n_total_data
        steps_per_epoch_validation = math.ceil(total_validation_samples / train_parameters.batch_size)

        if BACKEND == EBackends.PYTORCH:
            print(
                "The 'Estimated Total Size (MB)' is the size of the model without\n"
                "saved gradients for continuation of training. The gradients are\n"
                "saved automatically in addition, so the real size of the saved\n"
                "file is larger than stated.")
            print("----------------------------------------------------------------")

        print("Total training samples:", total_training_samples)
        print("Total validation samples:", total_validation_samples)
        print("Total samples (train & eval):", total_training_samples + total_validation_samples)
        print("----------------------------------------------------------------")
        print("Batch size:", train_parameters.batch_size)
        print("Steps per epoch (training):", train_parameters.steps_per_epoch)
        print("Steps per epoch (evaluation):", str(steps_per_epoch_validation))
        print("----------------------------------------------------------------")
        print("Each step per epoch, up to",
              train_parameters.batch_size,
              "samples will be processed. \nAt the end of each epoch (after " + str(train_parameters.steps_per_epoch),
              "training and\n" + str(steps_per_epoch_validation) + " validation steps, the total amount of",
              total_training_samples + total_validation_samples,
              "samples\nwill have been processed.\nThe last batch of each sample set (training and validation)\n"
              "can hold 1 to n items, with n = batch size.")
        print("_________________________________________________________________\n") \

    @staticmethod
    def _post_training_console_log(log_file_path):
        print("\n[INFO] Adding logged training information to\n" + log_file_path)
